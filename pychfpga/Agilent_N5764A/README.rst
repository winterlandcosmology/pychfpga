Agilent N5764A Power Supply Hardware Map object
===============================================

Implements a hardware map object that can control the CHIME power supply.

This is based on GPIB.py and agilent_N5700.py, which are snapshots from McGill's library of Python Lab
instrument drivers. They are found in McGill's Pylab repository, in the dev
branch.
