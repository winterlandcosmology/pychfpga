""" ORM object that describes the firmware that is associated with the FPGA.
"""
import logging
import hashlib
import zlib
import logging
import os.path
import struct
import urllib2
import datetime

from hardware_map import HWMResource, Integer, Column, String, Binary, LargeBinary, DateTime, ForeignKey, UniqueConstraint, reconstructor, inspect

class FPGABitstreamException(Exception):
    def __init__(self, message, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.error(message)
        super(type(self), self).__init(message, *args, **kwargs)

class HWMFPGABitstream(HWMResource):
    """ Represents the firmware that is running or to be run on the IceBoard FPGA.

    The firmware is represented by a URL in the database, and is cached in
    memory when the object is created by the user or when the get_data()
    method is called if it not already loaded.

    The bitstream is added to the database from a URI by get_bitstream().
    Normally, the bitstream data is only kept in memory and is not not stored in the database.
    If the parameter persist=True is provided, the bistream
    will also be stored in the database for future use.
    """

    __tablename__ = 'fpga_bitstream'
    # __mapper_args__ = {
    #         'polymorphic_on': 'firmware_class',
    #         'polymorphic_identity': 'fpgafirmware'
    # }
    __table_args__ = (
        UniqueConstraint('crc32'),
    )

    # Database columns
    pk = Column(Integer, primary_key=True)
    # polymorphic_class_name = Column(String, nullable=False) # String that identifies the polymorphic_identity of the class needed to handle the firmware
    class_name = Column(String, nullable=False) # String that identifies the Python class that operates with this FPGA firmware
    crc32 = Column(Integer)
    md5_string = Column(String)
    timestamp = Column(DateTime)
    url = Column(String) # URL where the binary can be found if not stored locally in the database
    bitstream = Column(LargeBinary)

    # Class attributes
    _active_instances = {} # class attributes indicating which instances have been created

    # Instance attributes
    bitstream_cache = None

    @classmethod
    def get_bitstream(cls, session, url=None, firmware_class = None, crc=None, persist=True):
        """ Returns a bitstream object from the database or create a new
        database entry if it does not exist yet. This function ensures that
        the bistream row is unique in the database, and will not cause
        set_fpga_firmware() to create new database entries concurrently.

        A new entry will be created only if both 'url' and 'firmware_class' is
        specified. However, if a bitstream with the same CRC exists in the
        database, the database entry will be updated and returned.

        Otherwise, the method will attempt to find a bitsream that
        match the search criteria:

            If 'url' is specified, it looks for a database entry that has the same CRC than the content of the file specified by 'url'
            If 'crc32' is specified, it looks for the database entry that has the specified CRC

        If multiple results are found, an error is raised.

        'url' can be an actual URL or a filename.
        """
        logger = logging.getLogger(__name__)

        new_bitstream_object = None

        # if an URL is specified, load it so we can inspect its CRC and eventually add it to the database.
        if url:
            new_bitstream_object = FpgaBitstream(url, firmware_class, persist = persist)
            crc = new_bitstream_object.crc32

        if crc:
            bitstream_objects = session.query(FpgaBitstream).filter_by(crc32 = crc) # find all entries with same CRC32

        number_of_results = bitstream_objects.count()

        if number_of_results>1:
            raise FpgaBitstreamException('The database contains multiple reference of firmware with CRC %08X. Using the first one.' % crc)
        if number_of_results:
            logger.info('The database contains already contains an entry with CRC %08X. Updating that one.' % crc)
            bitstream_object = bitstream_objects.first()
            if firmware_class:
                # bitstream_object.set_firmware_class(firmware_class)
                session.commit()
            return bitstream_object
        elif new_bitstream_object and firmware_class:
            logger.info('The bitstream with CRC %08X does not exist in the database. Adding it.' % crc)
            session.add(new_bitstream_object)
            session.commit()
            return new_bitstream_object
        else:
            raise FpgaBitstreamException('The bitstream with CRC %08X does not exist in the database, and a new entry cannot be created because of an invalid url or the firmware class was not specified.' % crc)

    def __init__(self, url, firmware_class=None, persist=True):
        """ Creates a bitstream object from the specified 'url'.
        If 'persist' is True, the bitstream will be stored in the database.
        """
        self.logger = logging.getLogger(__name__)

        self.url = url
        # If we manually create this bitstream object, we always fetch the
        # data immediately so we can get its CRC32 and other info and have a
        # cached version of it in memory.
        self.logger.info('Creating bitstream object explicitely')
        self._load_bitstream(self.url)

        # if firmware_class:
        #     self.set_firmware_class(firmware_class)
        self.class_name = firmware_class.__name__

        if persist:
            self.persist_bitstream()

        # fw = session.query(FpgaBitstream).filter_by(crc32 = bitstream_object.crc32) # find all entries with same CRC32
        # number_of_results = fw.count()

    @reconstructor
    def _init_from_database(self):
        """
        Re-create the firmware object from the database data.
        """
        #try to reload the bitstream
        self.logger = logging.getLogger(__name__)
        self.logger.info('Creating bitstream object from database')
        # we leave the local self.bistream set to None.
        # bitstream = FpgaBitFile(self.filename)
        # if bitstream.crc32 != self.bitstream.crc32:
        #     self.logger.warning('Bitstream does not have the expected CRC')
        # self.firmware_class = pickle.loads(self.firmware_class_pickle)
    def __repr__(self):
        return '<FpgaBitstream with CRC=%i>' % self.crc32

    def get_bitstream_data(self):
        """
        Fetch the bitstream from its storage (local cache, the database or the URL) and return it
        """
        if self.bitstream_cache:
            return self.bitstream_cache # return from the cache
        if self.bitstream:
            return self.bitstream # return from the database
        self.logger.info('Reloading during get_bitstream')
        self._load_bitstream(self.url)
        return self.bitstream_cache # return from the URL

    def persist_bitstream(self):
        """ Copy the cashed bitstream to the database"""

        self.logger.info('Persisting bitstream')
        if not self.bitstream_cache:
            self._load_bitstream(self.url)
        self.bitstream = self.bitstream_cache

    # def set_firmware_class(self, firmware_class):
    #     """ Set the database fields that allow the firmware handling class to be retreived.
    #     """
    #     polymorphic_identity = inspect(firmware_class).polymorphic_identity
    #     self.polymorphic_class_name = polymorphic_identity
    #     self.class_name = firmware_class.__name__


    # def get_firmware_class(self):
    #     """ Return the class object that should be used to access the firmware
    #     functionnalities corresponding to this bistream.
    #     """
    #     return inspect(FpgaCoreFirmware).polymorphic_map[self.polymorphic_class_name].class_

    def update(self, bitstream_object):
        """ Update the current object with the data contained with the provided one."""
        if bitstream_object.bitstream_cache:
            self.bitstream_cache = bitstream_object.bitstream_cache

    BIN_PREFIX = 0xffffffffaa995566

    def _load_bitstream(self, url):
        """
        Loads the bitstream contained by the URL into the cache memory and fill the corresponding info fields.

        Notes:
            BIT file format described in http://www.fpga-faq.com/FAQ_Pages/0026_Tell_me_about_bit_files.htm
        """
        # self.filename = filename
        timestamp = None
        md5_string = None

        self.logger.info('Reading file from URL %s ...' % url)
        if '://' in url:
            with urllib2.urlopen(url) as res:
                data = res.read()
        else:
             # Open as a file with relative path. mode='rb': b is important -> binary
             with open(url, 'rb') as file:
                data = file.read()
        self.logger.info('Read %0.3f Mbytes' % (len(data)/1e6))

        is_bin = struct.unpack('>Q',data[0:8])[0] == self.BIN_PREFIX

        if not is_bin:
            pos = 0
            # Field 1 - ignore
            length = struct.unpack('>H',data[pos:pos+2])[0]
            self.logger.debug('Field 1: 0x%s' % ''.join(['%0X' % ord(c) for c in data[pos+2:pos+2+length]]))
            pos += length + 2
            # Field 2 - always 'a'
            length = struct.unpack('>H',data[pos:pos+2])[0]
            field = data[pos+2:pos+2+length]
            self.logger.debug('Field 2 (%i bytes): %s' % (length,field))
            if field != 'a':
                self.logger.error('This is not a valid bit file')
                return
            pos += length + 2
            # Field 3
            length = struct.unpack('>H',data[pos:pos+2])[0]
            self.logger.debug('Field 3: %s' % data[pos+2:pos+2+length])
            pos += length + 2
            # Field 4
            tag = data[pos]
            length = struct.unpack('>H',data[pos+1:pos+2+1])[0]
            fpga_model = data[pos+2+1:pos+2+1+length]
            self.logger.debug('Field 4 (tag=%s, length = %i bytes): %s' % (tag, length, fpga_model))
            pos += length + 2 + 1
            # Field 5
            tag = data[pos]
            length = struct.unpack('>H',data[pos+1:pos+2+1])[0]
            firmware_date = data[pos+2+1:pos+2+1+length]
            self.logger.debug('Field 5 (tag=%s, length = %i bytes): %s' % (tag, length, firmware_date))
            pos += length + 2 + 1
            # Field 6
            tag = data[pos]
            length = struct.unpack('>H',data[pos+1:pos+2+1])[0]
            firmware_time = data[pos+2+1:pos+2+1+length]
            self.logger.debug('Field 6 (tag=%s, length = %i bytes): %s' % (tag, length, data[pos+2+1:pos+2+1+length]))
            pos += length + 2 + 1
            # Field 7
            tag = data[pos]
            length = struct.unpack('>L',data[pos+1:pos+4+1])[0]
            self.logger.debug('Field 7 (tag=%s, length= %i bytes): [configuration data]' % (tag, length))
            pos += 4 + 1 # skip the header. Now points to cofiguration data
            bitstream = data[pos:]

            timestamp_string = firmware_date + ' ' + firmware_time
            timestamp = datetime.datetime.strptime(firmware_date[:-1] + ' ' + firmware_time[:-1], '%Y/%m/%d %H:%M:%S')
            valid = True
        else:
            bitstream = data

        # compute MD5 sum as a hex string
        md5_string = hashlib.md5(bitstream).hexdigest()
        # compute CRC32 of the data
        crc32 = zlib.crc32(bitstream)

        self.crc32 = crc32
        self.bitstream_cache = bitstream
        self.timestamp_string = timestamp_string
        self.timestamp = timestamp
        self.md5_string = md5_string
        return

    def add_to_database(self, session):
        """
        Add the bitstream object to the database using the specified session. The object will be updated if the database contains one with the same CRC32.
        """
        fw = session.query(FpgaBitstream).filter_by(crc32 = self.crc32) # find all entries with same CRC32
        number_of_results = fw.count()
        if number_of_results>1:
            self.logger.warning('The database contains multiple reference of firmware with CRC %08X. Using the first one.' % self.crc32)
        if number_of_results:
            self.logger.info('The database contains already contains an entry with CRC %08X. Updating that one.' % self.crc32)
            fpga_bitstream = fw.first()
            fpga_bitstream.update(self) # update the database entry with the provided bitstream object and make sure the bistream is available
            session.commit()
        else:
            self.logger.info('The bitstream with CRC %08X does not exist in the database. Adding it.' % self.crc32)
            session.add(self)
            session.commit()
