pychfpga\.core\.chFPGA\_controller
==================================

.. currentmodule:: sphinx

.. autosummary::

   environment.BuildEnvironment
   util.relative_uri


.. automodule:: pychfpga.core.chFPGA_controller

   ..   :members:
   ..   :undoc-members:




   Summary
   -------

   .. autosummary::

      chFPGA_config
      chFPGA_controller

   Classes
   -------

..   .. autoclass:: chFPGA_controller
..
..      Some contents here in `sampling_frequency` ...
..
..      .. automethod:: init(*see below*)
..
..      .. automethod:: get_config (*see below*)
..
..
..   .. autoclass:: chFPGA_config

