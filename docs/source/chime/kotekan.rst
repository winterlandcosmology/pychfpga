:mod:`kotekan` module: GPU node correlator control process
==========================================================

.. .. automodule:: kotekan

.. .. autosummary::

	kotekan.KotekanAsyncRESTClient

kotekan REST Server
*******************

.. .. autoclass:: kotekan.KotekanAsyncRESTClient
..	:members:

