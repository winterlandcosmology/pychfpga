:class:`fpga_master.ChimeMaster`
================================

.. currentmodule:: fpga_master

.. autoclass:: ChimeMaster

.. autosummary::  ChimeMaster

   ChimeMaster


Methods
-------


Initialization
**************

.. automethod:: ChimeMaster.__init__
